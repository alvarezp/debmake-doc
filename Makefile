# Build all Debian packaging examples and produce documentation
# Copyright (C) 2015 Osamu Aoki

# Define if not defined (Prevent error when Make is run independently)
SOURCE_DATE_EPOCH ?= "1400000000"

# run "make prep" before "make log" to keep example package source tree consistent
# run "make logs" before "make all" to make logs for building example packages
# make sure to commit logs to the git repository as a part of the document source

# build document
all: build

# set common build parameters
include Makefile.common

# build rule for example package build logs
include Makefile.pkg

# build rule for documentation: asciidoc+log -> XML -> html/pdf
include Makefile.dbk

build: $(wildcard $(ASCIIDOC_DIR)/*.txt)
	# process ASCIIDOC after $(pkg) targets even under the parallel build
	$(MAKE) base-xml
	echo "LC_ALL='$(LC_ALL)'"
	echo "DEBUG='$(DEBUG)'"
	echo "LANGPO='$(LANGPO)'"
	echo "LANGALL='$(LANGALL)'"
	echo "NOPDF='$(NOPDF)'"
	echo "BASEDIR='$(BASEDIR)'"
	echo "TMPDIR='$(TMPDIR)'"
	$(MAKE) all-xml	                # XML docs for all PO
	-mkdir -p $(TMPDIR)
	-mkdir -p $(BASEDIR)/html
	# leave fuzzy status in the package build log
	-cat fuzzy.log

install: css html pdf epub txt # build docs from XML

clean::
	-rm -rf $(TMPDIR)
	-$(MAKE) -C $(ASCIIDOC_DIR) clean

distclean:: clean

# Use this target on devel branch source
package:
	debmake -t -y -zx -b':doc' -i pdebuild

.PHONY: all build install clean distclean package
